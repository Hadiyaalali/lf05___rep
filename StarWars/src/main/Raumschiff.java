package main;

import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	private static ArrayList<String> broadcastKommunikator;
	
	/**
	 * Erzeugt ein neues Raumschiff mit leerem Ladungsverzeichnis.
	 * Sämtliche Eigenschaften erhalten den Standardwert 0.
	 */
	public Raumschiff() {
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}

	/**
	 * Erzeugt ein neues Raumschiff mit den angegebenen Werten.
	 * @param photonentorpedoAnzahl die Photonentorpedo Anzahl des Raumschiffes
	 * @param energieversorgungInProzent die Energievorsorgung des Raumschiffes
	 * @param schildeInProzent der Zustand der Schilde des Raumschiffes
	 * @param huelleInProzent der Zustand der Hülle des Raumschiffes
	 * @param lebenserhaltungssystemeInProzent der Zustand der Lebenserhaltungssysteme des Raumschiffes
	 * @parae androidenAnzahl die Anzahl der Androiden auf dem Raumschiffes
	 * @param schiffsname der Name des Raumschiffes
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	

	/**
	 * @return the photonentorpedoAnzahl
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * @param photonentorpedoAnzahl the photonentorpedoAnzahl to set
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * @return the energieversorgungInProzent
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * @param energieversorgungInProzent the energieversorgungInProzent to set
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * @return the schildeInProzent
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * @param schildeInProzent the schildeInProzent to set
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * @return the huelleInProzent
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * @param huelleInProzent the huelleInProzent to set
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * @return the lebenserhaltungssystemeInProzent
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * @param lebenserhaltungssystemeInProzent the lebenserhaltungssystemeInProzent to set
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * @return the androidenAnzahl
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * @param androidenAnzahl the androidenAnzahl to set
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * @return the schiffsname
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * @param schiffsname the schiffsname to set
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/**
	 * @return the broadcastKommunikator
	 */
	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	/**
	 * @param broadcastKommunikator the broadcastKommunikator to set
	 */
	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	/**
	 * Die angegebene Ladung wird zum Ladungsverzeichnis hinzugefügt.
	 * @param ladung die Ladung
	 */
	public void addLadung(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}
	
	/**
	 * Voraussetzung: Wenn noch ein Photonentorpedo verfügbar ist,
	 * mit einem Photonentorpedo auf ein auf ein Ziel schießen. 
	 * Verringert die Anzahl der Photonentorpedos um 1.
	 * Sendet die Nachricht "Photonentorpedo abgeschossen" an alle, wenn der Abschuss möglich war, andernfalls die Nachricht "-=*Click*=-".
	 * Nutzt treffer() auf dem Ziel-Raumschiff, wenn geschossen werden konnte.
	 * @param ziel das Raumschiff, auf das geschossen werden soll
	 */
	public void photonentorpedoSchiessen(Raumschiff ziel) {
		if (this.photonentorpedoAnzahl > 0) {
			this.photonentorpedoAnzahl = this.photonentorpedoAnzahl - 1;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			ziel.treffer(this);
		} else {
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	/**
	 * Voraussetzung: Wenn die Energieversorgung noch über 50% beträgt,
	 * wird mit einer Phaserkanone auf ein Ziel geschossen.
	 * Verringert das Energielevel um 50%.
	 * Sendet die Nachricht "Phaserkanone abgeschossen" an alle, wenn der Abschuss möglich war, andernfall die Nachricht "-=*Click*=-".
	 * Nutzt treffer() auf dem Ziel-Raumschiff, wenn geschossen werden konnte.
	 * @param ziel das Raumschiff, auf das geschossen werden soll
	 */
	public void phaserkanoneSchiessen(Raumschiff ziel) {
		if (this.energieversorgungInProzent >= 50) {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			ziel.treffer(this);
		} else {
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * Zeigt den Text "[Schiffsname] wurde getroffen!" auf der Konsole an.
	 * [Schiffsname] wird durch den aktuellen Schiffsnamen ersetzt.
	 * @param angreifer das Raumschiff, das das aktuelle Raumschriff angreift
	 */
	private void treffer(Raumschiff angreifer) {
		String angegriffen = this.schiffsname + " wurde getroffen!";
		System.out.println(angegriffen);
	}
	
	/**
	 * Zeigt eine Nachricht auf der Konsole an.
	 * @param message die Nachricht, die angezeigt werden soll
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
	}
	
	public ArrayList<String> eingraegeLogbuchZurueckgeben() {
		return null;
	}
	
	public void photonentorpedosLaden(int anzahl) {
		
	}
	
	public void reperaturDurchfuehren(boolean schilde, boolean energie, boolean huelle, int droidenAnzahl) {
		
	}
	
	/**
	 * Zeigt den Zustand des Raumschiffes auf der Konsole an.
	 * Beispielausgabe:
	 * <pre>
	 * Schiff: Ni'Var
	 * Energieversorgung: 80%
	 * Huelle: 50%
	 * Lebenserhaltungssysteme: 100%
	 * Anzahl Photonentorpedos: 0
	 * Schilde: 80%
	 * Anzahl Androiden: 5
	 * Forschungssonde: 35
	 * Photonentorpedo: 3
	 * </pre>
	 */
	public void zustandRaumschiff() {
		String zustand = "Schiff: " + this.schiffsname + "\n";
		zustand = zustand + "Energieversorgung: " + this.energieversorgungInProzent + "%\n";
		zustand = zustand + "Huelle: " + this.huelleInProzent + "%\n";
		zustand = zustand + "Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent + "%\n";
		zustand = zustand + "Anzahl Photonentorpedos: " + this.photonentorpedoAnzahl + "\n";
		zustand = zustand + "Schilde: " + this.schildeInProzent + "%\n";
		zustand = zustand + "Anzahl Androiden: " + this.androidenAnzahl;
		System.out.println(zustand);
	}
	
	/**
	 * Zeigt das Ladungsverzeichnis des Raumschiffes auf der Konsole an.
	 * Beispielausgabe:
	 * <pre>
	 * Plasma-Waffe: 50
	 * Borg-Schrott: 5
	 * Rote Materie: 2
	 * </pre>
	 */
	public void ladungsverzeichnisAusgeben() {
		for (Ladung l : this.ladungsverzeichnis) {
			String anzeige = l.getBezeichnung();
			anzeige = anzeige + ": ";
			anzeige = anzeige + l.getMenge();
			System.out.println(anzeige);
		}
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
	}
	
}
