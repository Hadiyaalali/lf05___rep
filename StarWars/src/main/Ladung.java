package main;

public class Ladung {
	private String bezeichnung;
	private int menge;
	
	/**
	 * Erzeugen einer Ladung ohne Namen und mit der Menge 0.
	 */
	public Ladung() {
		
	}
	
	/**
	 * Erzeugen einer Ladung mit festgelegter Bezeichnung und Menge
	 * 
	 * @param bezeichnung die Bezeichnung der Ladung
	 * @param menge die Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		super();
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 * @return the bezeichnung
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * @param bezeichnung the bezeichnung to set
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * @return the menge
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * @param menge the menge to set
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	
}
